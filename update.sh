#!/bin/sh
echo "Start downloading latest filterlists"
curl https://easylist-downloads.adblockplus.org/easylist-minified.txt --output Sources/easylist-minified.txt
curl https://easylist-downloads.adblockplus.org/exceptionrules-minimal.txt --output Sources/exceptionrules-minimal.txt
curl https://easylist-downloads.adblockplus.org/easylistchina-minified.txt --output Sources/easylistchina-minified.txt
curl https://easylist-downloads.adblockplus.org/easylistdutch-minified.txt --output Sources/easylistdutch-minified.txt
curl https://easylist-downloads.adblockplus.org/easylistgermany-minified.txt --output Sources/easylistgermany-minified.txt
curl https://easylist-downloads.adblockplus.org/easylistitaly-minified.txt --output Sources/easylistitaly-minified.txt
curl https://easylist-downloads.adblockplus.org/easylistspanish-minified.txt --output Sources/easylistspanish-minified.txt
curl https://easylist-downloads.adblockplus.org/liste_fr-minified.txt --output Sources/liste_fr-minified.txt
curl https://easylist-downloads.adblockplus.org/ruadlist-minified.txt --output Sources/ruadlist-minified.txt
curl https://raw.githubusercontent.com/abp-filters/abp-filters-compliance/main/germany.txt --output Sources/germany-compliance.txt
echo "Finished downloading latest filterlists"

# Replace the header [Adblock Plus 2.0] with ![Adblock Plus 2.0] to ensure it doesnt get picked up as a rule in error.
echo "Start formating file headers"
perl -pi -w -e 's/\[Adblock Plus 2.0]/! \[Adblock Plus 2.0]/g;' Sources/*.txt
echo "Finish formating file headers"

echo "Start copy & combine easylist filterlists"
cat Sources/easylist-minified.txt > Sources-Combined/easylist-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistchina-minified.txt > Sources-Combined/easylist+easylistchina-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistdutch-minified.txt > Sources-Combined/easylist+easylistdutch-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistgermany-minified.txt newline.txt Sources/germany-compliance.txt > Sources-Combined/easylist+easylistgermany-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistitaly-minified.txt > Sources-Combined/easylist+easylistitaly-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistspanish-minified.txt > Sources-Combined/easylist+easylistspanish-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/liste_fr-minified.txt > Sources-Combined/easylist+liste_fr-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/ruadlist-minified.txt > Sources-Combined/easylist+ruadlist-minified.txt
cat Sources/easylist-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistchina-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+easylistchina-minified+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistdutch-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+easylistdutch-minified+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistgermany-minified.txt newline.txt Sources/exceptionrules-minimal.txt newline.txt Sources/germany-compliance.txt > Sources-Combined/easylist+easylistgermany-minified+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistitaly-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+easylistitaly-minified+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/easylistspanish-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+easylistspanish-minified+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/liste_fr-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+liste_fr-minified+exceptionrules-minimal.txt
cat Sources/easylist-minified.txt newline.txt Sources/ruadlist-minified.txt newline.txt Sources/exceptionrules-minimal.txt > Sources-Combined/easylist+ruadlist-minified+exceptionrules-minimal.txt
echo "Finish copy & combine easylist filterlists"

echo "Starting abp2blocklist conversion"
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist-minified.txt > Pre-Rulefix/easylist_content_blocker.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistchina-minified.txt > Pre-Rulefix/easylist+easylistchina-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistdutch-minified.txt > Pre-Rulefix/easylist+easylistdutch-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistgermany-minified.txt > Pre-Rulefix/easylist+easylistgermany-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistitaly-minified.txt > Pre-Rulefix/easylist+easylistitaly-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistspanish-minified.txt > Pre-Rulefix/easylist+easylistspanish-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+liste_fr-minified.txt > Pre-Rulefix/easylist+liste_fr-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+ruadlist-minified.txt > Pre-Rulefix/easylist+ruadlist-minified.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+exceptionrules-minimal.txt > Pre-Rulefix/easylist+exceptionrules_content_blocker.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistchina-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+easylistchina-minified+exceptionrules-minimal.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistdutch-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+easylistdutch-minified+exceptionrules-minimal.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistgermany-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+easylistgermany-minified+exceptionrules-minimal.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistitaly-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+easylistitaly-minified+exceptionrules-minimal.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+easylistspanish-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+easylistspanish-minified+exceptionrules-minimal.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+liste_fr-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+liste_fr-minified+exceptionrules-minimal.json
node abp2blocklist/abp2blocklist.js < Sources-Combined/easylist+ruadlist-minified+exceptionrules-minimal.txt > Pre-Rulefix/easylist+ruadlist-minified+exceptionrules-minimal.json
echo "Finished abp2blocklist conversion"

echo "Start CBRuleRepair"
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist_content_blocker.json Post-Rulefix/easylist_content_blocker.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+exceptionrules_content_blocker.json Post-Rulefix/easylist+exceptionrules_content_blocker.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistchina-minified.json  Post-Rulefix/easylist+easylistchina-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistdutch-minified.json Post-Rulefix/easylist+easylistdutch-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistgermany-minified.json Post-Rulefix/easylist+easylistgermany-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistitaly-minified.json Post-Rulefix/easylist+easylistitaly-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistspanish-minified.json  Post-Rulefix/easylist+easylistspanish-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+liste_fr-minified.json Post-Rulefix/easylist+liste_fr-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+ruadlist-minified.json Post-Rulefix/easylist+ruadlist-minified.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistchina-minified+exceptionrules-minimal.json Post-Rulefix/easylist+easylistchina-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistdutch-minified+exceptionrules-minimal.json  Post-Rulefix/easylist+easylistdutch-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistgermany-minified+exceptionrules-minimal.json Post-Rulefix/easylist+easylistgermany-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistgermany-minified+exceptionrules-minimal.json Post-Rulefix/easylist+easylistgermany-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistitaly-minified+exceptionrules-minimal.json Post-Rulefix/easylist+easylistitaly-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+easylistspanish-minified+exceptionrules-minimal.json Post-Rulefix/easylist+easylistspanish-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+liste_fr-minified+exceptionrules-minimal.json Post-Rulefix/easylist+liste_fr-minified+exceptionrules-minimal.json
node cbrulefixer/cbrulerepair.js Pre-Rulefix/easylist+ruadlist-minified+exceptionrules-minimal.json Post-Rulefix/easylist+ruadlist-minified+exceptionrules-minimal.json
echo "Finish CBRuleRepair"
echo "Conversion is now complete"

